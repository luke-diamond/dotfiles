if [ "$TERM" = "linux" ]; then
    echo -en "\e]P0151515" #black
    echo -en "\e]P82B2B2B" #darkgrey
    echo -en "\e]P1D75F5F" #darkred
    echo -en "\e]P9AC4142" #red
    echo -en "\e]P287AF5F" #darkgreen
    echo -en "\e]PA90A959" #green
    echo -en "\e]P3D7AF87" #brown
    echo -en "\e]PBFFD75F" #yellow
    echo -en "\e]P48787AF" #darkblue
    echo -en "\e]PC6A9FB5" #blue
    echo -en "\e]P5AA759F" #darkmagenta
    echo -en "\e]PDD633B2" #magenta
    echo -en "\e]P66A9FB5" #darkcyan
    echo -en "\e]PE75B5AA" #cyan
    echo -en "\e]P7CCCCCC" #lightgrey
    echo -en "\e]PFF5F5F5" #white
    clear #for background artifacting
fi
