bar {
    status_command "i3status"
    position top
    font pango:Hack-Regular 8
    colors {
        background #151515
        statusline #aaaaaa
        focused_workspace #90a959 #90a959 #151515
        inactive_workspace #151515 #151515 #aaaaaa
    }
}
