zstyle ':completion:*' menu select

autoload -Uz compinit
compinit

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
export PATH=$HOME/.cargo/bin:$PATH
export MAKEFLAGS='-j5'
export PS1="%F{green}#%f "
export RPS1="%F{green}%BD:%2d | J:%j | U:%n%b%f"

alias ls="ls --color=always"
