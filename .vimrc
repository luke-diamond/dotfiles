filetype off

call plug#begin('~/.local/share/nvim/plugged')

Plug 'rust-lang/rust.vim'
Plug 'cespare/vim-toml'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/nerdtree'
Plug 'morhetz/gruvbox'
Plug 'jistr/vim-nerdtree-tabs'

call plug#end()
filetype plugin indent on

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#right_sep = ' '
let g:airline#extensions#tabline#right_alt_sep = '|'
let g:airline_theme='monochrome'
let g:onedark_termcolors=16

autocmd BufWritePre * :silent! %s/\s\+$/
set number
set runtimepath+=~/.vim
set guifont=PowerlineSymbols
set guioptions-=m
set guioptions-=T
set guioptions-=e
set guioptions-=r
set nosmartindent
set tabstop=4
set shiftwidth=4
set expandtab
set colorcolumn=80

map <M-up> :silent! :m--<CR>
map <M-k> :silent! :m--<CR>
map <M-down> :silent! :m+<CR>
map <M-j> :silent! :m+<CR>
nmap <C-n> :silent! :noh<CR>
nmap <C-left> :silent! :tabn-<CR>
nmap <C-S-left> :silent! :tabm-<CR>
nmap <C-right> :silent! :tabn+<CR>
nmap <C-S-right> :silent! :tabm+<CR>
nmap <C-a> :silent! :NERDTreeTabsToggle<CR>

colorscheme peachpuff
